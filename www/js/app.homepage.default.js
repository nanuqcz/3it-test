jQuery(function($){
	var initRowSelection = function(){
		$('table.items tbody tr').click(function(){
	        $(this).toggleClass('success');
	    });
	};
	initRowSelection();

	// Ajax support
	$.nette.ext('rowSelection', {
		success: initRowSelection,
	});
});
