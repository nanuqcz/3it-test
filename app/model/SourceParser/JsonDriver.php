<?php
namespace App\Model\SourceParser;

use Nette;


/**
 * JsonDriver
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class JsonDriver implements IDriver
{

    public function parse($file)
    {
        $content = file_get_contents($file);
        $data = json_decode($content);

        return $data;
    }

}
