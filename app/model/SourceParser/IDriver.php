<?php
namespace App\Model\SourceParser;

use Nette;


/**
 * IDriver
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
interface IDriver
{

    /**
     * Parse input file
     * @param  string $file  full path to the file with data
     * @return array         parsed data
     */
    public function parse($file);

}
