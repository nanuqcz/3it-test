<?php
namespace App\Model\SourceParser\Exceptions;

use Exception;


/**
 * DriverNotFound
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class DriverNotFound extends Exception
{
}
