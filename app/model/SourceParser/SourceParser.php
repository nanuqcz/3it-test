<?php
namespace App\Model\SourceParser;

use Nette,
    Nette\Utils\Strings,
    Nette\Utils\FileSystem;


/**
 * Parser
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class Parser
{
    /** @var string */
    private $downloadDir;

    /** @var array */
    private $drivers = [];


    public function __construct($downloadDir)
    {
        $this->downloadDir = $downloadDir;
    }


    public function registerDriver($name, $driver)
    {
        $this->drivers[$name] = $driver;
    }


    public function parse($source, $driverName)
    {
        // Download file
        $file = $this->download($source);

        // Load driver
        if (empty($this->drivers[$driverName])) {
            throw new Exceptions\DriverNotFound;
        }
        $driver = $this->drivers[$driverName];

        // Parse
        $data = $driver->parse($file);

        return $data;
    }


    private function download($url)
    {
        // Prepare download
        $filename = Strings::webalize($url, '.');
        $downloadPath = "$this->downloadDir/$filename." . date('Y-m-d_H-i-s');

        FileSystem::createDir($this->downloadDir);  // make sure the folder exists

        // Do it!
        file_put_contents(
            $downloadPath,
            fopen($url, 'r')  // TODO exception?
        );

        return $downloadPath;
    }

}
