<?php
namespace App\Model;

use Nette,
    Nette\Database;


/**
 * Repository
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class Repository
{
    /** @var Database\Context */
    private $db;

    public function __construct(Database\Context $db)
    {
        $this->db = $db;
    }


    /**
     * @param  array $order  order for SQL in format [column, type]
     * @param  array $limit  limit for SQL in format [limit, offset]
     * @return Selection     list of the result rows
     */
    public function getList($order = NULL, $limit = NULL)
    {
        $selection = $this->db->table('zaznamy');

        if ($order) {
            $selection = $selection->order("$order[0] $order[1]");
        }

        if ($limit) {
            $selection = $selection->limit($limit[0], $limit[1]);
        }

        return $selection;
    }


    public function import($data)
    {
        $this->clear();
        $this->db->table('zaznamy')
            ->insert($data);
    }


    private function clear()
    {
        return $this->db->table('zaznamy')
            ->delete();
    }

}
