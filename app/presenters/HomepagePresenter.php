<?php
namespace App\Presenters;

use App\Model\SourceParser\Parser,
    App\Model\Repository;
use IPub\VisualPaginator\Components as VisualPaginator;
use Nette,
    Nette\Application\UI\Form;


/**
 * Homepage presenter
 *
 * @author  Michal Mikolas <nanuqcz@gmail.com>
 */
class HomepagePresenter extends Nette\Application\UI\Presenter
{
    /** @persistent */
    public $itemsPerPage = 10;

    /** @persistent */
    public $orderBy = 'date';

    /** @persistent */
    public $orderType = 'ASC';

    /** @inject @var Parser */
    public $sourceParser;

    /** @inject @var Repository */
    public $repository;



    // #
    // #       #  ####  #####
    // #       # #        #
    // #       #  ####    #
    // #       #      #   #
    // #       # #    #   #
    // ####### #  ####    #
    public function renderDefault()
    {
        // Get data
        $items = $this->repository->getList(
            [$this->orderBy, $this->orderType]
        );

        // Paginate
        $count = count($items);
		$paginator = $this['paginator']->getPaginator();
		$paginator->itemCount = $count;

		$itemsPaginated = $this->repository->getList(
            [$this->orderBy, $this->orderType],
			[$paginator->itemsPerPage, $paginator->offset]
		);

        // Render
        $this->template->items = $items;
        $this->template->itemsPaginated = $itemsPaginated;

        if ($this->isAjax()) {
            $this->redrawControl('items');
        }
    }


    /**
	 * @return VisualPaginator\Control
	 */
	protected function createComponentPaginator()
	{
		$control = new VisualPaginator\Control;
		$control->getPaginator()->itemsPerPage = 10;

		$control->setTemplateFile('bootstrap.latte');

		$control->getPaginator()->itemsPerPage = $this->itemsPerPage;

		return $control;
	}


	public function handleSetItemsPerPage($itemsPerPage)
	{
		$this->itemsPerPage = $itemsPerPage;

		$this->redirect('this');
	}


    public function handleChangeOrder($column)
    {
        if ($this->orderBy != $column) {
            $this->orderBy = $column;
            $this->orderType = 'ASC';
        } else {
            $this->orderType = ($this->orderType == 'ASC')? 'DESC': 'ASC';
        }

        $this->redirect('this');
    }


    // ###
    //  #  #    # #####   ####  #####  #####
    //  #  ##  ## #    # #    # #    #   #
    //  #  # ## # #    # #    # #    #   #
    //  #  #    # #####  #    # #####    #
    //  #  #    # #      #    # #   #    #
    // ### #    # #       ####  #    #   #
    protected function createComponentImportForm()
    {
        $form = new Form;
        $form->addText('source', 'Zdroj')
            ->setRequired('Vyplň prosím hodnotu pole "%label".');
        $form->addSelect('driver', 'Ovladač', [
            'json' => 'JSON',
        ])->setRequired('Vyplň prosím hodnotu pole "%label".');
        $form->addSubmit('process', 'Importuj!!!');

        $form->onSuccess[] = array($this, 'processImportForm');
        return $form;
    }


    public function processImportForm(Form $form)
    {
        $values = $form->values;

        $data = $this->sourceParser->parse($values->source, $values->driver);
        $this->repository->import($data);

        $this->flashMessage('Import dokončen 😊', 'success');
        $this->redirect('this');
    }

}
